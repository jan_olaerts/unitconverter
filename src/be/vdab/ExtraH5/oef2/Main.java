package be.vdab.ExtraH5.oef2;

import java.util.Formatter;
import java.util.Scanner;

public class Main {

    public static Scanner scanner = new Scanner(System.in);
    public static Formatter formatter = new Formatter();

    public static void main(String[] args) {

        System.out.println("Choose the type of conversion you want to make: ");

        System.out.println("1. Euro -> Dollar");
        System.out.println("2. Celsius -> Fahrenheit");
        System.out.println("3. Meter -> Feet");
        System.out.println("4. Kilometers - > Miles");

        int choice = scanner.nextInt();

        switch(choice) {
            case 1:
                euroToDollar();
                break;
            case 2:
                celsiusToFahrenheit();
                break;
            case 3:
                metersToFeet();
                break;
            case 4:
                kmToMiles();
                break;
        }
    }

    public static void euroToDollar() {

        System.out.println("Give an amount in Euro");
        while(true) {

            try {
                double euro = scanner.nextDouble();
                double dollar = euro * 1.14;
                formatter.format(euro + " euros = %.2f dollar", dollar);
                System.out.println(formatter.toString());
                break;
            } catch(RuntimeException e) {
                System.out.println("Enter a valid amount: ");
            } finally {
                scanner.nextLine();
            }
        }
    }

    public static void celsiusToFahrenheit() {
        System.out.println("Give degrees in Celsius");
        while(true) {

            try {
                double celsius = scanner.nextDouble();
                double fahrenheit;
                if(celsius == 0) {
                    fahrenheit = 32;
                } else {
                    fahrenheit = celsius * 9 / 5 + 32;
                }
                formatter.format(celsius + " degrees Celsius = %.2f degrees Fahrenheit", fahrenheit);
                System.out.println(formatter.toString());
                break;
            } catch(RuntimeException e) {
                System.out.println("Enter a valid amount of degrees: ");
            } finally {
                scanner.nextLine();
            }
        }
    }

    public static void metersToFeet() {
        System.out.println("Give a distance in meters: ");
        while(true) {

            try {
                double meters = scanner.nextDouble();
                double feet = meters * 3.28;
                formatter.format(meters + " meters = %.2f feet", feet);
                System.out.println(formatter.toString());
                break;
            } catch(RuntimeException e) {
                System.out.println("Enter a valid distance in meters: ");
            } finally {
                scanner.nextLine();
            }
        }
    }

    public static void kmToMiles() {

        System.out.println("Give a distance in kilometers: ");
        while(true) {

            try {
                double km = scanner.nextDouble();
                double miles = km * 0.62;
                formatter.format(km + " kilometers = %.2f miles", miles);
                System.out.println(formatter.toString());
                break;
            } catch(RuntimeException e) {
                System.out.println("Enter a valid number: ");
            } finally {
                scanner.nextLine();
            }
        }
    }
}